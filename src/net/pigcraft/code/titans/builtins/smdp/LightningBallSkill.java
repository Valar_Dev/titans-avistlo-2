package net.pigcraft.code.titans.builtins.smdp;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.entity.Snowball;

public final class LightningBallSkill extends TitanSkill
{
    public LightningBallSkill(final Titan titan)
    {
        super("ElectroBall", titan, new Cooldown(5));

        this.setDescription("Shoots a ball of electricity at foe");
    }

    @Override
    public void use()
    {
        final Snowball lightningBall = this.getTitan().getController().getWorld().spawn(this.getTitan().getController().getLocation(), Snowball.class);
        lightningBall.setShooter(this.getTitan().getController());
        lightningBall.setVelocity(this.getTitan().getController().getLocation().getDirection().multiply(1.5));
    }
}
