package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titans;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public final class PlayerDeathListener implements Listener
{

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent e)
    {
        if (Titans.plugin.isPlayerATitan(e.getEntity()))
        {
            Titans.plugin.removePlayerAsTitan(e.getEntity());
        }
    }

}
