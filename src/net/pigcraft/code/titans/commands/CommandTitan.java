package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class CommandTitan implements CommandExecutor
{

    protected static final String SYNTAX_ERROR = ChatColor.GRAY + "Error in command syntax! Use " + ChatColor.RED + "/titan help" + ChatColor.GRAY + " for help.";

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
    {
        /*if (command.getName().equalsIgnoreCase("titan"))
        {

            if (args.length > 0)
            {
                if (args[0].equalsIgnoreCase("on"))
                {

                    if (!(commandSender instanceof Player))
                    {
                        commandSender.sendMessage(ChatColor.RED + "I'm sorry, but you must be a player to do that!");
                        return false;
                    }

                    if (args.length > 1)
                    {
                        Titan titan = Titans.plugin.getTitan(args[1]);

                        if (titan == null)
                        {
                            commandSender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[1] + ChatColor.GRAY + " is not a Titan!");
                            return false;
                        }

                        Titans.plugin.setPlayerAsTitan((Player)commandSender, titan);
                        return true;
                    }
                    else
                    {
                        commandSender.sendMessage(SYNTAX_ERROR);
                        return false;
                    }
                }
                else if (args[0].equalsIgnoreCase("off"))
                {
                    if (!(commandSender instanceof Player))
                    {
                        commandSender.sendMessage(ChatColor.RED + "I'm sorry, but you must be a player to do that!");
                        return false;
                    }

                    Titans.plugin.removePlayerAsTitan((Player)commandSender);
                    return true;
                }
                else if (args[0].equalsIgnoreCase("skill") || args[0].equalsIgnoreCase("use"))
                {
                    if (args.length > 1)
                    {

                        if (!(commandSender instanceof Player))
                        {
                            commandSender.sendMessage(ChatColor.RED + "I'm sorry, but you must be a player to do that!");
                            return false;
                        }

                        if (Titans.plugin.isPlayerATitan((Player)commandSender))
                        {
                            TitanSkill skill = Titans.plugin.getTitanAssignedToPlayer((Player)commandSender).getSkill(args[1]);

                            if (skill == null)
                            {
                                commandSender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[1] + ChatColor.GRAY +  " is not a skill for your current Titan!");
                                return false;
                            }

                            if (skill.getCooldown().done())
                            {
                                skill.use();
                                skill.getCooldown().start();
                            }
                            else
                            {
                                commandSender.sendMessage(ChatColor.GRAY + "Error, skill " + ChatColor.AQUA + skill.getName() + ChatColor.GRAY + " is on cooldown for "
                                		+ ChatColor.RED + skill.getCooldown().timeLeft() +ChatColor.GRAY + " more seconds");
                            }

                        }
                        else
                        {
                            commandSender.sendMessage(ChatColor.RED + "You are not a Titan!");
                            return false;
                        }
                    }
                    else
                    {
                    	if (Titans.plugin.isPlayerATitan((Player)commandSender))
                    	{
                    		commandSender.sendMessage(ChatColor.RED + "Incorrect Command Usage, You did not enter a skill name \n "
                    		+ ChatColor.DARK_GRAY +"Correct usage is" + ChatColor.AQUA + " /titan <use|skill> <skillName>");
                            for (TitanSkill skill : Titans.plugin.getTitanAssignedToPlayer((Player)commandSender).getSkills())
                            {
                                commandSender.sendMessage("\n" + ChatColor.AQUA + "/titan skill " + skill.getName());
                                commandSender.sendMessage("       " + ChatColor.AQUA + skill.getDescription());
                            }
                    	}
                        else
                        {
                            commandSender.sendMessage(ChatColor.RED + "You are not a Titan!");
                            return false;
                        }
                    }
                    return false;
                }
                else if (args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("ls"))
                {
                    commandSender.sendMessage(ChatColor.GRAY + "Total loaded Titans: " + ChatColor.AQUA + Titans.getLoadedTitans().size());

                    for (Titan t : Titans.getLoadedTitans())
                    {
                        commandSender.sendMessage("    " + ChatColor.AQUA + t.getName());

                        if (t.hasController())
                        {
                            commandSender.sendMessage("        " + ChatColor.GRAY + "Controlled by : " + ChatColor.AQUA + t.getController().getDisplayName());
                        }
                    }

                    return true;
                }
                else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?"))
                {
                    commandSender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + "Help for Titans" + ChatColor.GRAY + " ====");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan on " + ChatColor.DARK_AQUA + "<titan name> ");
                    commandSender.sendMessage(ChatColor.GRAY + "    Transforms you into the specified Titan.");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan off");
                    commandSender.sendMessage(ChatColor.GRAY + "    Returns you to your normal state.");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan list");
                    commandSender.sendMessage(ChatColor.GRAY + "    Lists all available Titans and who's controlling them.");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan skill " + ChatColor.DARK_AQUA + "<skill name>");
                    commandSender.sendMessage(ChatColor.GRAY + "    Use a skill for your current Titan.");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan lore " + ChatColor.DARK_AQUA + "<Titan name>");
                    commandSender.sendMessage(ChatColor.GRAY + "    Lists information about the titan.");

                    commandSender.sendMessage(ChatColor.AQUA + "/titan info " + ChatColor.DARK_AQUA + "<Titan name>");
                    commandSender.sendMessage(ChatColor.GRAY + "    Lists information about the titan. Staff use.");
                    return true;

                }
                else if (args[0].equalsIgnoreCase("lore"))
                {

                    if (args.length > 1)
                    {
                    	Titan t = Titans.plugin.getTitan(args[1]);

                        if (t == null)
                        {
                            commandSender.sendMessage(ChatColor.GRAY + "Titan " + ChatColor.RED + args[1] + ChatColor.GRAY + " doesn't exist!");
                            return false;
                        }


                        commandSender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + t.getName() + ChatColor.GRAY + " ====");
                        commandSender.sendMessage("");

                        if (!(t.getDescription().equals("")))
                        {
                            commandSender.sendMessage(ChatColor.AQUA + t.getDescription());
                        }

                        commandSender.sendMessage("");

                        if (t.hasController())
                        {
                        	//Possible thing to add to Titan.java, if they are online, what should the message be, or the will the player not know if the titan is online.
                            commandSender.sendMessage(ChatColor.GRAY + t.getName() + " is currently roaming the world, watch out!");
                        }
                        else
                        {
                        	//Possible thing to add to titan.java, if offline, what to say here.
                        	commandSender.sendMessage(ChatColor.GRAY + t.getName() + " is nowhere to be found, you may feel safe now.");
                        }
                    }
                    else
                    {
                    	commandSender.sendMessage(ChatColor.RED + "Incorrect usage of the command! ");
                    	commandSender.sendMessage(ChatColor.GRAY + "Correct usage is "+ ChatColor.AQUA + "/titan lore " +
                    	ChatColor.DARK_AQUA + "<Titan Name>");

                    }
                        return true;


                    }
                else if (args[0].equalsIgnoreCase("info"))
                {

                    if (args.length > 1)
                    {
                        Titan t = Titans.plugin.getTitan(args[1]);

                        if (t == null)
                        {
                            commandSender.sendMessage(ChatColor.GRAY + "Titan " + ChatColor.RED + args[1] + ChatColor.GRAY + " doesn't exist!");
                            return false;
                        }


                        commandSender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + t.getName() + ChatColor.GRAY + " ====");
                        commandSender.sendMessage("");

                        if (!(t.getDescription().equals("")))
                        {
                            commandSender.sendMessage(ChatColor.AQUA + t.getDescription());
                        }

                        commandSender.sendMessage("");

                        if (t.hasController())
                        {
                            commandSender.sendMessage(ChatColor.GRAY + "Currently controlled by " + ChatColor.AQUA + t.getController().getName());
                        }

                        commandSender.sendMessage(ChatColor.GRAY + "Skills:");

                        for (TitanSkill skill : t.getSkills())
                        {
                            commandSender.sendMessage(ChatColor.GRAY + "    Name: " + ChatColor.AQUA + skill.getName());
                            commandSender.sendMessage(ChatColor.GRAY + "    Info: " + ChatColor.AQUA + skill.getDescription());
                        }

                        return true;


                    }
                    else
                    {
                    	commandSender.sendMessage(ChatColor.RED + "Incorrect usage of the command! ");
                    	commandSender.sendMessage(ChatColor.GRAY + "Correct usage is "+ ChatColor.AQUA + "/titan info"
                    	+ ChatColor.DARK_AQUA + " <Titan Name>");

                    }

                    return false;
                }
                else
                {
                    commandSender.sendMessage(SYNTAX_ERROR);
                    return false;
                }
            }
            else
            {
                commandSender.sendMessage(SYNTAX_ERROR);
                return false;
            }
        }
        else
        {
            return false;
        }*/

        if (command.getName().equalsIgnoreCase("titan"))
        {
            if (args.length > 0)
            {
                for (ITitanSubCommand cmd : Titans.getTitanCommands())
                {
                    if (cmd.getName().equalsIgnoreCase(args[0]))
                    {
                        cmd.onCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
                        return true;
                    }
                    else if(cmd.getAltName().equalsIgnoreCase(args[0]))
                    {
                        cmd.onCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
                        return true;                    	
                    }
                }

                commandSender.sendMessage(ChatColor.GRAY + "Could not find command " + ChatColor.RED + args[0]);
                return false;
            }
            else
            {
                commandSender.sendMessage(SYNTAX_ERROR);
                return false;
            }
        }
        else
        {
            return false;
        }

    }


}
