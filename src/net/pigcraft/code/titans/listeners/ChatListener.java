package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titans;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public final class ChatListener implements Listener
{

    @EventHandler(priority = EventPriority.HIGHEST)
    public final void onPlayerChat(AsyncPlayerChatEvent event)
    {
        if (Titans.plugin.isPlayerATitan(event.getPlayer()))
        {
            event.setFormat(Titans.plugin.getTitanAssignedToPlayer(event.getPlayer()).getDisplayFormat().replace("$m", event.getMessage()));
        }
    }

}
