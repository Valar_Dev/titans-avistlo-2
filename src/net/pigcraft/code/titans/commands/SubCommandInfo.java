package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class SubCommandInfo implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (args.length == 1)
        {
            Titan t = Titans.plugin.getTitan(args[0]);

            if (t == null)
            {
                sender.sendMessage(ChatColor.GRAY + "Titan " + ChatColor.RED + args[0] + ChatColor.GRAY + " doesn't exist!");
                return false;
            }


            sender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + t.getName() + ChatColor.GRAY + " ====");
            sender.sendMessage("");

            if (!(t.getDescription().equals("")))
            {
                sender.sendMessage(ChatColor.AQUA + t.getDescription());
            }

            sender.sendMessage("");

            if (t.hasController())
            {
                sender.sendMessage(ChatColor.GRAY + "Currently controlled by " + ChatColor.AQUA + t.getController().getName());
            }

            sender.sendMessage(ChatColor.GRAY + "Skills:");

            for (TitanSkill skill : t.getSkills())
            {
                sender.sendMessage(ChatColor.GRAY + "    Name: " + ChatColor.AQUA + skill.getName());
                sender.sendMessage(ChatColor.GRAY + "    Info: " + ChatColor.AQUA + skill.getDescription());
            }

            return true;


        }
        else
        {
            sender.sendMessage(ChatColor.RED + "Incorrect usage of the command! ");
            sender.sendMessage(ChatColor.GRAY + "Correct usage is "+ ChatColor.AQUA + "/titan info"
                    + ChatColor.DARK_AQUA + " <Titan Name>");

        }

        return false;
    }

    @Override
    public String getName()
    {
        return "info";
    }

	@Override
	public String getAltName() {
		return "online";
	}
}
