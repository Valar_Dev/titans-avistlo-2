package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class SubCommandLore implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (args.length == 1)
        {
            Titan t = Titans.plugin.getTitan(args[0]);
            if (t == null)
            {
                sender.sendMessage(ChatColor.GRAY + "Titan " + ChatColor.RED + args[0] + ChatColor.GRAY + " doesn't exist!");
                return false;
            }


            sender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + t.getName() + ChatColor.GRAY + " ====");
            sender.sendMessage("");

            if (!(t.getDescription().equals("")))
            {
                sender.sendMessage(ChatColor.AQUA + t.getDescription());
            }

            sender.sendMessage("");

            if (t.hasController())
            {
                //Possible thing to add to Titan.java, if they are online, what should the message be, or the will the player not know if the titan is online.
                sender.sendMessage(ChatColor.GRAY + t.getName() + " is currently roaming the world, watch out!");
            }
            else
            {
                //Possible thing to add to titan.java, if offline, what to say here.
                sender.sendMessage(ChatColor.GRAY + t.getName() + " is nowhere to be found, you may feel safe now.");
            }
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "Incorrect usage of the command! ");
            sender.sendMessage(ChatColor.GRAY + "Correct usage is "+ ChatColor.AQUA + "/titan lore " +
                    ChatColor.DARK_AQUA + "<Titan Name>");

        }

        return false;
    }

    @Override
    public String getName()
    {
        return "lore";
    }

	@Override
	public String getAltName() {
		return "history";
	}
}
