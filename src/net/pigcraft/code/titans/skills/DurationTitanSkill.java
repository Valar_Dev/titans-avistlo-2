package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;

public abstract class DurationTitanSkill extends TitanSkill
{
    
    public DurationTitanSkill(final String name, final Titan titan, final Cooldown cooldown)
    {
    	super(name, titan, cooldown);
    }

    public DurationTitanSkill(final String name, final Titan titan)
    {
    	super(name, titan);
    }
    
    public abstract void use();
    
	public abstract void DurationDone();

}
