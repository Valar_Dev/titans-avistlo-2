package net.pigcraft.code.titans.builtins.avistlo;

//import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
//import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

public final class ContradictFateSkill extends TitanSkill
{
    public ContradictFateSkill(final Titan titan)
    {
        super("Fate", titan, new Cooldown(1));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Contradict Fate"
        + "\n           Avistlo undoes the Fate of past beings and resurects" + 
        		"\n           monsters.");
    }

    @SuppressWarnings("deprecation")
	@Override
    public void use()
    {
    	Player controller = this.getTitan().getController();
    	Location loc = this.getTitan().getController().getLocation();
    	
    	controller.getWorld().spawnEntity(controller.getTargetBlock(null, 200).getLocation(), EntityType.ZOMBIE);
    	controller.getWorld().spawnEntity(controller.getTargetBlock(null, 200).getLocation(), EntityType.SKELETON);
    	
    	Location spawn1 = new Location(loc.getWorld(), loc.getX() - 1,loc.getY(), loc.getZ());
    	Location spawn2 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() + 1);
    	Location spawn3 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() - 1);
    	Location spawn4 = new Location(loc.getWorld(), loc.getX() + 1,loc.getY(), loc.getZ());
		controller.getWorld().spawnEntity(spawn1, EntityType.ZOMBIE);
		controller.getWorld().spawnEntity(spawn2, EntityType.ZOMBIE);
		controller.getWorld().spawnEntity(spawn3, EntityType.SKELETON);
		controller.getWorld().spawnEntity(spawn4, EntityType.SKELETON);
		//@ me
		//Try looking at https://forums.bukkit.org/threads/how-to-make-a-mob-not-target-a-specific-player.105868/
		// and https://forums.bukkit.org/threads/player-get-no-damage-from-zombies.163892/
		//and https://forums.bukkit.org/threads/how-to-get-entitys-to-try-to-find-a-target-on-a-command.49598/
		// to try and figure out how to get mobs to ignore avistlo. Currently they spawn around him, but then attack avistlo making it useless
		//there is a simple solution to cancel the damage to avistlo, but canceling targeting might be complex.
		//Look back at those links when you get time.
    }
   
}
