package net.pigcraft.code.titans.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class SubCommandHelp implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {

        sender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + "Help for Titans" + ChatColor.GRAY + " ====");

        sender.sendMessage(ChatColor.AQUA + "/titan on " + ChatColor.DARK_AQUA + "<titan name> ");
        sender.sendMessage(ChatColor.GRAY + "    Transforms you into the specified Titan.");

        sender.sendMessage(ChatColor.AQUA + "/titan off");
        sender.sendMessage(ChatColor.GRAY + "    Returns you to your normal state.");

        sender.sendMessage(ChatColor.AQUA + "/titan list");
        sender.sendMessage(ChatColor.GRAY + "    Lists all available Titans and who's controlling them.");

        sender.sendMessage(ChatColor.AQUA + "/titan skill " + ChatColor.DARK_AQUA + "<skill name>");
        sender.sendMessage(ChatColor.GRAY + "    Use a skill for your current Titan.");

        sender.sendMessage(ChatColor.AQUA + "/titan lore " + ChatColor.DARK_AQUA + "<Titan name>");
        sender.sendMessage(ChatColor.GRAY + "    Lists information about the titan.");

        sender.sendMessage(ChatColor.AQUA + "/titan info " + ChatColor.DARK_AQUA + "<Titan name>");
        sender.sendMessage(ChatColor.GRAY + "    Lists information about the titan. Staff use.");
        return true;
        
    }

    @Override
    public String getName()
    {
        return "help";
    }

	@Override
	public String getAltName() {
		return "?";
	}

}
