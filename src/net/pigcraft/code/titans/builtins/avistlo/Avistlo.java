package net.pigcraft.code.titans.builtins.avistlo;

import java.util.ArrayList;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public final class Avistlo extends Titan
{

    private double prevHealth;
    ArrayList<EntityType> MobProtections;
    ArrayList<DamageCause> WorldProtections;
    
    public Avistlo()
    {
        super
        (
                Titans.plugin,
                "Avistlo", "Fate and Destiny begin to change as Avistlo appears",
                "Everything calms as Avistlo vanishes from the world.",
                ChatColor.DARK_GRAY + "Skylord Avistlo " + ChatColor.GRAY + "$m"
        );

        this.setDescription(" Avistlo is the Titan of Light and Fate." +
        " Brother to the Titan of Reality and Life," +
        " he came into existence near the beginning of time." + 
        " Avistlo travels the world and skies, changing"
        +"the Fate of those he finds based on the Light around them."+
        " He is known to give gifts or curses when sought out so be wary should you ever meet.");
        
        MobProtections = new ArrayList<EntityType>();
        MobProtections.add(EntityType.ZOMBIE);
        MobProtections.add(EntityType.CREEPER);
        MobProtections.add(EntityType.SKELETON);
        MobProtections.add(EntityType.WITHER_SKULL);
        MobProtections.add(EntityType.WITHER);
        MobProtections.add(EntityType.ENDER_DRAGON);
        MobProtections.add(EntityType.SLIME);
        MobProtections.add(EntityType.SILVERFISH);
        MobProtections.add(EntityType.ENDERMAN);
        MobProtections.add(EntityType.CAVE_SPIDER);
        MobProtections.add(EntityType.ENDER_CRYSTAL);
        MobProtections.add(EntityType.SPIDER);
        this.SetMobProtectionList(MobProtections);
        
        WorldProtections = new ArrayList<DamageCause>();
        WorldProtections.add(DamageCause.LIGHTNING);
        WorldProtections.add(DamageCause.FIRE);
        WorldProtections.add(DamageCause.FIRE_TICK);
        WorldProtections.add(DamageCause.FALL);
        WorldProtections.add(DamageCause.FALLING_BLOCK);
        WorldProtections.add(DamageCause.VOID);
        this.SetWorldProtectionList(WorldProtections);
    }

    @Override
    public void onEntrance()
    {
    	
        for (Player player : this.getController().getWorld().getPlayers())
        {
            this.getController().getWorld().playSound(player.getLocation(), Sound.AMBIENCE_CAVE, 1, 0);
        }
        this.getController().setFireTicks(0);
        this.prevHealth = this.getController().getMaxHealth();
        this.getController().setMaxHealth(160D);
        this.getController().setHealth(160D);
        this.getController().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2000000, 1));
        this.getController().getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
        //check that for working with armor inv's
        //http://jd.bukkit.org/rb/apidocs/org/bukkit/inventory/PlayerInventory.html#getArmorContents()
    }

    @Override
    public void onExit()
    {
        for (Player player : this.getController().getWorld().getPlayers())
        {
            this.getController().getWorld().playSound(player.getLocation(), Sound.AMBIENCE_THUNDER, 1, 0);
        }
        
        this.getController().setHealth(this.prevHealth);
        this.getController().setMaxHealth(this.prevHealth);
        this.getController().removePotionEffect(PotionEffectType.JUMP);
        //The next line is just a proof of concept. Most Likely will make an array of getArmorContents 
        //OnEntrancer, and restore it here.
        this.getController().getInventory().setHelmet(null);
    }

    @Override
    public void onLoad()
    {
    	this.addSkill(new GiftOfFateSkill(this));
    	this.addSkill(new ForceOfLight(this));
    	this.addSkill(new ContradictFateSkill(this));
    	this.addSkill(new WindsOfChangeSkill(this));
    }

    @Override
    public void onUnload()
    {
    	this.clearSkills();
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
