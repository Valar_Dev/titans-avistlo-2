package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandSkill implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (args.length == 1)
        {

            if (!(sender instanceof Player))
            {
                sender.sendMessage(ChatColor.RED + "I'm sorry, but you must be a player to do that!");
                return false;
            }

            if (Titans.plugin.isPlayerATitan((Player)sender))
            {
                TitanSkill skill = Titans.plugin.getTitanAssignedToPlayer((Player)sender).getSkill(args[0]);

                if (skill == null)
                {
                    sender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[0] + ChatColor.GRAY +  " is not a skill for your current Titan!");
                    return false;
                }

                if (skill.getCooldown().done())
                {
                    skill.use();
                    skill.getCooldown().start();
                }
                else
                {
                    sender.sendMessage(ChatColor.GRAY + "Error, skill " + ChatColor.AQUA + skill.getName() + ChatColor.GRAY + " is on cooldown for "
                            + ChatColor.RED + skill.getCooldown().timeLeft() +ChatColor.GRAY + " more seconds");
                }

            }
            else
            {
                sender.sendMessage(ChatColor.RED + "You are not a Titan!");
                return false;
            }
        }
        else
        {
            if (Titans.plugin.isPlayerATitan((Player)sender))
            {
                sender.sendMessage(ChatColor.RED + "Incorrect Command Usage, You did not enter a skill name \n "
                        + ChatColor.DARK_GRAY +"Correct usage is" + ChatColor.AQUA + " /titan <use|skill> <skillName>");
                for (TitanSkill skill : Titans.plugin.getTitanAssignedToPlayer((Player)sender).getSkills())
                {
                    sender.sendMessage("\n" + ChatColor.AQUA + "/titan skill " + skill.getName());
                    sender.sendMessage("       " + ChatColor.AQUA + skill.getDescription());
                }
            }
            else
            {
                sender.sendMessage(ChatColor.RED + "You are not a Titan!");
                return false;
            }
        }
        return false;
    }

    @Override
    public String getName()
    {
        return "skill";
    }

	@Override
	public String getAltName() {
		return "use";
	}
}
