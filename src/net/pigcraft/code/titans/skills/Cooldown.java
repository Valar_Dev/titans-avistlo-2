package net.pigcraft.code.titans.skills;

/**
 * @author Blockynight (IRL Haskell friend/Math guy)
 */
public final class Cooldown
{

    private long time;
    private long timeout;

    public Cooldown(final long timeout)
    {
        this.timeout = timeout * 1000;
    }

    public final void start()
    {
        this.time = System.currentTimeMillis();
    }
    
    //timeLeft() is authored by Dev. Only called on during time in between start and done.
    public final long timeLeft()
    {
		return (((System.currentTimeMillis() - (this.time + this.timeout)) / 1000) * -1) ;
    	
    }

    public final boolean done()
    {
        return System.currentTimeMillis() >= this.time + this.timeout;
    }




}
