package net.pigcraft.code.titans;

import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public abstract class Titan
{

    protected final Titans plugin;

    private Player controller;
    private String name;
    private String entranceMessage;
    private String exitMessage;
    private String displayFormat;
    private String description;

    private final ArrayList<TitanSkill> skills;
    private final ArrayList<org.bukkit.entity.EntityType> mobProtectionList;
    private final ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> worldProtectionList;
    
    public abstract void onEntrance();
    public abstract void onExit();

    public abstract void onLoad();
    public abstract void onUnload();

    public Titan(final Titans plugin, String name, String entranceMessage, String exitMessage, String displayFormat)
    {
        this.plugin = plugin;
        this.name = name;
        this.entranceMessage = entranceMessage;
        this.exitMessage = exitMessage;
        this.displayFormat = displayFormat;
        this.description = "";

        this.skills = new ArrayList<TitanSkill>();
        this.mobProtectionList = new ArrayList<org.bukkit.entity.EntityType>();
        this.worldProtectionList = new ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause>();
    }

    public final String getName()
    {
        return name;
    }

    public final Player getController()
    {
        return controller;
    }

    public final String getEntranceMessage()
    {
        return entranceMessage;
    }

    public final String getExitMessage()
    {
        return exitMessage;
    }

    public final String getDisplayFormat()
    {
        return displayFormat;
    }

    public final String getDescription()
    {
        return description;
    }

    public final ArrayList<org.bukkit.entity.EntityType> getMobProtectionList()
    {
    	return mobProtectionList;
    }

    public final ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> getWorldProtectionList()
    {
    	return worldProtectionList;
    }

    public final void addSkill(final TitanSkill skill)
    {
        if (this.skills.contains(skill))
        {
            Bukkit.getLogger().severe("Titan " + this.getName() + "tried to register duplicate skill " + skill.getName());
        }
        else
        {
            this.skills.add(skill);
        }
    }

    public final TitanSkill getSkill(final String skillName)
    {
        for (TitanSkill skill : this.skills)
        {
            if (skill.getName().equalsIgnoreCase(skillName))
            {
                return skill;
            }
        }

        //I know I am being inefficient here, but if they use altNames how they should be used, accidents, then it'l be fine.
        for (TitanSkill skill : this.skills)
        {
        	if(skill.getAltNames() != null)
        	{
        		for(String s : skill.getAltNames())
        		{
        			if (skillName.equalsIgnoreCase(s))
        				return skill;
        		}
        	}
        }
        return null;
    }

    public final void clearSkills()
    {
        if (this.skills != null)
        {
            this.skills.clear();
        }
    }

    public final ArrayList<TitanSkill> getSkills()
    {
        return this.skills;
    }




    public final void setController(final Player controller)
    {
        this.controller = controller;
    }

    public final void setName(String name)
    {
        this.name = name;
    }

    public final void setEntranceMessage(final String entranceMessage)
    {
        this.entranceMessage = entranceMessage;
    }

    public final void setExitMessage(final String exitMessage)
    {
        this.exitMessage = exitMessage;
    }

    public final void setDisplayFormat(final String displayFormat)
    {
        this.displayFormat = displayFormat;
    }

    public final boolean hasController()
    {
        return this.controller != null;
    }

    public final void setDescription(final String description)
    {
        this.description = description;
    }

    public final void SetWorldProtectionList(ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> WorldProtections)
    {
    	this.worldProtectionList.addAll(WorldProtections);
    }    

    public final void SetMobProtectionList(ArrayList<org.bukkit.entity.EntityType> MobProtections)
    {
    	this.mobProtectionList.addAll(MobProtections);
    }


    public static Titan fromFile(final File file, final URLClassLoader loader) throws
            FileNotFoundException,
            NoSuchMethodException,
            ClassNotFoundException
    {
        Titan returnable = null;
        InputStream in = null;
        JarFile jar = null;

        try
        {
            jar = new JarFile(file);

            final JarEntry desc = jar.getJarEntry("titan.properties");
            final String[] titanProperties = new String[6];

            if (desc == null)
            {
                throw new FileNotFoundException("Jar " + jar.getName() + " does not contain a titan.properties file!");
            }

            final Properties props = new Properties();
            in = jar.getInputStream(desc);
            props.load(in);

            titanProperties[0] = props.getProperty("main");
            titanProperties[1] = props.getProperty("lore");
            titanProperties[2] = props.getProperty("enterMsg");
            titanProperties[3] = props.getProperty("exitMsg");
            titanProperties[4] = props.getProperty("name");
            titanProperties[5] = props.getProperty("chatFormat");

            final Class<? extends Titan> titan = loader.loadClass(titanProperties[0]).asSubclass(Titan.class);
            final Constructor<? extends Titan> constructor = titan.getConstructor();

            returnable = constructor.newInstance();
            returnable.setDescription(titanProperties[1]);
            returnable.setEntranceMessage(titanProperties[2]);
            returnable.setExitMessage(titanProperties[3]);
            returnable.setName(titanProperties[4]);
            returnable.setDisplayFormat(ChatColor.translateAlternateColorCodes('&', titanProperties[5]));

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }

                if (jar != null)
                {
                    jar.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        return returnable;

    }


}
