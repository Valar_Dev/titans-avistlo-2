package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandOff implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "We're sorry, but you must be a player to do that!");
            return false;
        }

        Titans.plugin.removePlayerAsTitan((Player)sender);
        return true;
    }

    @Override
    public String getName()
    {
        return "off";
    }

	@Override
	public String getAltName() {
		return "leave";
	}

}
