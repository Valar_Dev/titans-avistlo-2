package net.pigcraft.code.titans;

import net.pigcraft.code.titans.builtins.avistlo.Avistlo;
import net.pigcraft.code.titans.commands.*;
import net.pigcraft.code.titans.listeners.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class Titans extends JavaPlugin
{
    /**
     * Singleton instance
     */
    public static Titans plugin = null;

    private static ArrayList<Titan> loadedTitans = new ArrayList<Titan>();
    private static ArrayList<Player> titanPlayers = new ArrayList<Player>();

    private static ArrayList<ITitanSubCommand> titanCommands = new ArrayList<ITitanSubCommand>();

    private static File titansDir = null;

    @Override
    public void onEnable()
    {
        plugin = this;
        titansDir = new File(this.getDataFolder() + File.separator + "ExtMods");

        // If only we could use NIO2 ):
        if (!titansDir.exists())
        {
            if (!titansDir.mkdirs())
            {
                Bukkit.getLogger().severe("[Titans] Critical - Failed to create Titans/ExtMods dir!");
            }
        }

        this.loadTitans();

        for (Titan t : getLoadedTitans())
        {
            t.onLoad();
        }

        this.getCommand("titan").setExecutor(new CommandTitan());
        this.getServer().getPluginManager().registerEvents(new ChatListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
        this.getServer().getPluginManager().registerEvents(new TitanProtectionListeners(), this);

        titanCommands.add(new SubCommandHelp());
        titanCommands.add(new SubCommandOn());
        titanCommands.add(new SubCommandOff());
        titanCommands.add(new SubCommandList());
        titanCommands.add(new SubCommandLore());
        titanCommands.add(new SubCommandInfo());
        titanCommands.add(new SubCommandSkill());
    }

    @Override
    public void onDisable()
    {

        for (Titan t : getLoadedTitans())
        {

            if (t.hasController())
            {
                t.getController().sendMessage(ChatColor.AQUA + "[Titans] " + ChatColor.GRAY + "You have been forced out" +
                        " of Titan form by a reload!");
                this.removePlayerAsTitan(t.getController());
            }

            t.onUnload();
        }

        loadedTitans.clear();
        titanPlayers.clear();
    }


    private void loadTitans()
    {

        loadedTitans.add(new Avistlo());

        if (titansDir != null)
        {
            final ArrayList<File> jars = new ArrayList<File>();

            for (String s : titansDir.list())
            {
                if (s.endsWith(".jar"))
                {
                    jars.add(new File(titansDir, s));
                }
            }

            final URL[] urls = new URL[jars.size()];

            for (int i = 0; i < jars.size(); i++)
            {
                try
                {
                    urls[i] = jars.get(i).toURI().toURL();
                }
                catch (MalformedURLException ex)
                {
                    ex.printStackTrace();
                }
            }

            final URLClassLoader classLoader = new URLClassLoader(urls, this.getClassLoader());

            for (File f : jars)
            {
                try
                {
                    getLoadedTitans().add(Titan.fromFile(f, classLoader));
                }
                catch (NoSuchMethodException ex)
                {
                    Bukkit.getLogger().severe("[Titans] Unexpected exception while loading jar " + f.getName() +
                            ", constructor is not valid");
                    ex.printStackTrace();
                }
                catch (ClassNotFoundException ex)
                {
                    Bukkit.getLogger().severe("[Titans] Unexpected exception while loading jar " + f.getName() +
                            ", main class not found");
                    ex.printStackTrace();
                }
                catch (FileNotFoundException ex)
                {
                    Bukkit.getLogger().severe("[Titans] Unexpected exception while loading jar " + f.getName() +
                            ", description file not found");
                    ex.printStackTrace();
                }
            }


        }
    }

    public final boolean isPlayerATitan(final Player player)
    {
        return titanPlayers.contains(player);
    }

    public final Titan getTitanAssignedToPlayer(final Player player)
    {
        for (Titan t : loadedTitans)
        {
            if (t.hasController())
            {
                if (t.getController().equals(player))
                {
                    return t;
                }
            }
        }

        return null;
    }


    public void setPlayerAsTitan(final Player player, final Titan titan)
    {
        if (!this.isPlayerATitan(player))
        {

            if (titan.hasController())
            {
                player.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Sorry, but that Titan is already " +
                        "controlled by " + ChatColor.RED + titan.getController().getDisplayName());
                return;
            }

            Bukkit.getServer().broadcastMessage(titan.getEntranceMessage());
            titan.setController(player);

            titan.getController().sendMessage(ChatColor.GRAY + "You have been transformed into the Titan " + ChatColor.AQUA + titan.getName() + "!");
            titanPlayers.add(player);
            titan.onEntrance();
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are already a titan!");
        }
    }

    public void removePlayerAsTitan(final Player player)
    {
        if (this.isPlayerATitan(player))
        {

            Titan titan = getTitanAssignedToPlayer(player);

            Bukkit.getServer().broadcastMessage(titan.getExitMessage());
            titan.onExit();


            titanPlayers.remove(player);
            titan.getController().sendMessage(ChatColor.GREEN + "You are no longer a Titan!");


            titan.setController(null);

        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are not a titan!");
        }
    }


    public Titan getTitan(final String titanName)
    {
        for (int i = 0; i < getLoadedTitans().size(); i++)
        {
            if (getLoadedTitans().get(i).getName().equalsIgnoreCase(titanName))
            {
                return getLoadedTitans().get(i);
            }
        }

        return null;
    }

    public static ArrayList<Titan> getLoadedTitans()
    {
        return loadedTitans;
    }

    public static ArrayList<ITitanSubCommand> getTitanCommands()
    {
        return titanCommands;
    }




}
